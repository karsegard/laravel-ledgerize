<?php

// config for KDA/Laravel\Ledgerize
return [
    'tables'=>[
        'transactions'=>'transactions',
        'types'=>'types'
    ]
];
