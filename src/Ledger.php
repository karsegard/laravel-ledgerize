<?php

namespace KDA\Laravel\Ledgerize;

use KDA\Laravel\Ledgerize\Facades\Ledger as FacadesLedger;
use KDA\Laravel\Ledgerize\Models\Type as ModelsType;
use KDA\Laravel\Ledgerize\Types\Type;
use ReflectionClass;

//use Illuminate\Support\Facades\Blade;
class Ledger
{
    protected array $types = [];

    public function registerType(string | object $type,$key=null){
        $type = is_object($type)? get_class($type): $type;

        $key = blank($key) ? $type::getKey() : $key;

        if(!isset($this->types[$key])){
            $reflect = new ReflectionClass($type);
            $t = ModelsType::firstOrCreate(
                ['class'=>$type,'key'=>$key],
                ['class'=>$type,'key'=>$key,'name'=>$reflect->getShortName()],
            );
            $this->types[$key]=$t;
        }else {
            throw new \Exception('Transaction type with key '.$key.' already registered');
        }
    }

    public function getType($key){
        return $this->types[$key]??null;
    }


    public function credit (Model $model,float $amount, string | Type $type, $group=''){
        return $this->factory()->model($model)->type($type)->group($group)->credit($amount);
    }

    public function debit (Model $model,float $amount, string | Type $type, $group=''){
        return $this->factory()->model($model)->type($type)->group($group)->debit($amount);
    }

    public function factory(){
        return app()->make(TransactionFactory::class);
    }

    public function watchAttribute(Model $model,$attribute, string | Type $type, string $group = ''){

        $model::updating(function($model) use ($attribute){
            if($model->isDirty($attribute)){
                $actual = $model->$attribute;
                $original = $model->getOriginal($attribute,$type,$group);
                if($actual < $original){
                    FacadesLedger::debit($model,$original-$actual,$type,$group);
                }else if ($actual > $original){
                    FacadesLedger::credit($model,$original-$actual,$type,$group);
                }
            }
        });

        $model::created(function($model) use ($attribute,$type,$group){
                $actual = $model->$attribute;
                if($actual < 0){
                    FacadesLedger::debit($model,$actual,$type,$group);
                }else if ($actual > 0){
                    FacadesLedger::credit($model,$actual,$type,$group);
                }
        });
    }

}
