<?php

namespace KDA\Laravel\Ledgerize\Facades;

use Illuminate\Support\Facades\Facade;

class Ledger extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
