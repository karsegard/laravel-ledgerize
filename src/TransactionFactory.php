<?php
namespace KDA\Laravel\Ledgerize;

use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Traits\Conditionable;
use Illuminate\Support\Traits\Tappable;
use KDA\Laravel\Ledgerize\Concerns\EvaluatesClosure;
use KDA\Laravel\Ledgerize\Models\Transaction;
use KDA\Laravel\Ledgerize\Models\Type;
use KDA\Laravel\Ledgerize\Types\Type as TypesType;

class TransactionFactory{

    use Tappable;
    use Conditionable;
    use EvaluatesClosure;

    
    protected Model | Closure | null $origin=null;

    public function origin(Model | Closure $origin): static
    {
        $this->origin = $origin;
        return $this;
    }
    public function getOrigin ():?Model
    {
        return $this->evaluate($this->origin);
    }

    protected Model | Closure $model;

    public function model(Model | Closure $model): static
    {
        $this->model = $model;
        return $this;
    }
    public function getModel():Model{
        return $this->evaluate($this->model);
    }

    protected Type | Closure $type;

    public function type(Type | TypesType | Closure $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getType():Type{
        $type = $this->evaluate($this->type);
        // todo retrieve model type
        return $type;
    }

    protected string $group='';

    public function group(string | Closure $group=''):static
    {
        $this->group = $group;
        return $this;
    }

    public function getGroup():string{
        return $this->evaluate($this->group);
    }
    

    public function debit($amount){
        
    }

    public function create(float $amount):Transaction
    {
        $model = $this->getModel();
        $origin = $this->getOrigin();
        $type = $this->getType();
        $group = $this->getGroup();
        $attributes = collect([
            'model_type'=>get_class($model),
            'model_id'=>$model->getKey(),
            'amount'=>$amount ,  
            'type_id'=>$type->getKey(),
        ])->when(!blank($origin),function($collection) use ($origin){
            $collection->put('origin_id',$origin->getKey());
            $collection->put('origin_type',get_class($origin));
        })->when(!blank($group),function($collection) use ($group){
            $collection->put('group',$group);
        });
        return Transaction::create($attributes->toArray());
    }

    public function __construct(){

    }
}