<?php

namespace KDA\Laravel\Ledgerize\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'model_id',
        'type_id',
        'model_type',
        'origin_id',
        'origin_type',
        'amount'
    ];

    protected $appends = [

    ];

    protected $casts = [

    ];

    protected static function newFactory()
    {
        return  \KDA\Laravel\Ledgerize\Database\Factories\TransactionFactory::new();
    }

    /*
    public function getTable()
    {

    }
*/
}
