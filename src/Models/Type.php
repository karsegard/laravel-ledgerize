<?php

namespace KDA\Laravel\Ledgerize\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;

    protected $fillable = [
        'key',
        'class',
        'name'
    ];

    protected $appends = [

    ];

    protected $casts = [

    ];

    protected static function newFactory()
    {
        return  \KDA\Laravel\Ledgerize\Database\Factories\TypeFactory::new();
    }

    /*
    public function getTable()
    {

    }
*/
}
