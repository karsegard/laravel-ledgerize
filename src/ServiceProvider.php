<?php

namespace KDA\Laravel\Ledgerize;

use KDA\Laravel\Ledgerize\Facades\Ledger as Facade;
use KDA\Laravel\Ledgerize\Ledger as Library;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasConfigurableTableNames;
use KDA\Laravel\Traits\HasLoadableMigration;
use KDA\Laravel\Traits\HasMigration;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasLoadableMigration;
    use HasMigration;
    use HasConfigurableTableNames;

    protected $packageName = 'laravel-ledgerize';

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

     // trait \KDA\Laravel\Traits\HasConfig;
     //    registers config file as
     //      [file_relative_to_config_dir => namespace]
    protected $configDir = 'config';

    protected $configs = [
        'kda/ledgerize.php' => 'kda.ledgerize',
    ];

    protected static $tables_config_key = 'kda.ledgerize.tables';

    //  trait \KDA\Laravel\Traits\HasLoadableMigration
    //  registers loadable and not published migrations
    // protected $migrationDir = 'database/migrations';
    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            return new Library();
        });
    }

    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }

    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
