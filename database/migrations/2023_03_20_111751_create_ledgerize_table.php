<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use KDA\Laravel\Ledgerize\ServiceProvider;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create(ServiceProvider::getTableName('transactions'), function (Blueprint $table) {
            $table->id();
            $table->numericMorphs('model');
            $table->bigInteger('amount');
            $table->boolean('credit')->default(false);
            $table->boolean('debit')->default(false);
            $table->nullableNumericMorphs('origin');
            $table->foreignId('type_id');
            $table->string('group')->default('');
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists(ServiceProvider::getTableName('transactions'));

        Schema::enableForeignKeyConstraints();
    }
};
