<?php

namespace KDA\Laravel\Ledgerize\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use KDA\Laravel\Ledgerize\Models\Type;

class TypeFactory extends Factory
{
    protected $model = Type::class;

    public function definition()
    {
        return [
            //
        ];
    }
}
