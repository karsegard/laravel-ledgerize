Feature: Creating transaction
   Background: Create base info
        Given the model class is "KDA\Tests\Models\Post"
        And crafting a record 
        And registering a transaction type "\KDA\Tests\TransactionType\TestTransactionType" for key "inventory"
        And using crafted model as transaction model

    Scenario: Transaction Can be created
        Then A transaction of "100"  can be created
        And the transaction is not null

    Scenario: Transaction Can be created with group
        Then A transaction of "100"  can be created
        And the transaction is not null