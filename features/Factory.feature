Feature: TransactionModel
    Background: Customer
    Given the model class is "KDA\Laravel\Ledgerize\Models\Transaction"
    And the following factory attributes 
    """
    {
        "model_id":"1",
        "model_type":"test",
        "amount":10,
        "type_id":1

    }
    """
    

    Scenario: Model can be created without any attribute
    Given crafting a record 
    Then The record is not null
