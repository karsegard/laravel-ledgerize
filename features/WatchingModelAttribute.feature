Feature: Watching Model Attributes
    Background: 
   # Given the transaction class is "KDA\Laravel\Ledgerize\Models\"
    Given the model class is "KDA\Tests\Models\Post"
    And crafting a record 
    

    Scenario: Model can be created without any attribute
    Given we watch the attribute "stock" with type key "inventory" in group "stock" on the created record
    
    Then The record is not null
