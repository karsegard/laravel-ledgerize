<?php

namespace KDA\Tests\Behat\Context\Concerns;

use KDA\Laravel\Ledgerize\Facades\Ledger;

/**
 * Defines application features from the specific context.
 */
trait Transaction
{

    protected $transaction_model = null;
    protected $transaction_type = null;
    protected $transaction_group = null;
    protected $transaction = null;
    /**
     * @Given registering a transaction type :class for key :key
     */
    public function registeringATransactionTypeForKey($class, $key)
    {
        Ledger::registerType($class,$key);
        $this->transaction_type = Ledger::getType($key);
        
    }
/**
     * @Then the transaction type is not null
     */
    public function theTransactionTypeIsNotNull()
    {
       $this->assertNotNull( $this->transaction_type);
    }

     /**
     * @Given using crafted model as transaction model
     */
    public function usingCraftedModelAsTransactionModel()
    {
        $this->transaction_model = $this->factory_record;
    }
/**
     * @Then A transaction of :amount  can be created
     */
    public function aTransactionOfCanBeCreated($amount)
    {
       $this->transaction = Ledger::factory()->model($this->transaction_model)->type($this->transaction_type)->create($amount);
    }
     /**
     * @Then the transaction is not null
     */
    public function theTransactionIsNotNull()
    {
        $this->assertNotNull($this->transaction);
    }
    /**
     * @Given the transaction class is :arg1
     */
    public function theTransactionClassIs($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Given we watch the attribute :attribute with type key :type_key in group :group
     */
    public function weWatchTheAttributeWithTypeKeyInGroup($attribute, $type_key, $group)
    {
       // Ledger::watchAttribute($this->transaction_model)
    }

        /**
     * @Given we watch the attribute :attribute with type key :type_key in group :group on the created record
     */
    public function weWatchTheAttributeWithTypeKeyInGroupOnTheCreatedRecord($attribute, $type_key, $group)
    {
      
    }
}