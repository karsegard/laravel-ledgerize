<?php

namespace KDA\Tests\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use KDA\Tests\Database\Factories\UserFactory;

/**
 * @mixin IdeHelperUser
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasFactory;

    protected $fillable = ['name', 'email', 'password'];

    protected $searchableFields = ['*'];

    protected $hidden = [
        'password',
        'remember_token',

    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Add a mutator to ensure hashed passwords
     */
    public function setPasswordAttribute($value)
    {
        if (\Hash::needsRehash($value)) {
            //dd('hashed_password');
            $value = \Hash::make($value);
        } else {
            // dd('not hashed',$value);
        }
        $this->attributes['password'] = $value;
    }

    protected static function newFactory()
    {
        return  UserFactory::new();
    }
}
